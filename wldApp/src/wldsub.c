#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>

//Global variable
int Debug;

static long getwaterdata(aSubRecord *precord)
{
	char *lvala;
	int *state, *value;
	int pclose_status=0;

	FILE *fp;
	char cmd[128];
	char resp[512];
	double upt;
	int year,month,day,hour,min,sec,aux;

        lvala=precord->vala;
        state=precord->valb;
        value=precord->valc;

	sprintf(cmd, "snmpget -v1 -cpublic -Ov -t 0.2 172.31.180.76 .1.3.6.1.2.1.1.3.0 .1.3.6.1.4.1.21796.4.5.4.1.3.1 .1.3.6.1.4.1.21796.4.5.4.1.6.1");

	fp = popen(cmd, "r");
	if(fp == NULL){
		printf("aSub Err: popen failed\n");
		*state=2;
		*value=0;
   	}else{
		fgets(resp, sizeof(resp)-1, fp);
		sscanf(resp, "%*s %*c%lf", &upt);
		if(Debug) printf("uptime is: %lf\n", upt);
                fgets(resp, sizeof(resp)-1, fp);
		sscanf(resp, "%*s %d", state);
                if(Debug) printf("state is: %d\n", *state);
                fgets(resp, sizeof(resp)-1, fp);
		sscanf(resp, "%*s %d", value);
                if(Debug) printf("value is: %d\n", *value);

		aux = (int)floor(upt/100.0);
		year = floor(aux/31104000);
		aux = aux % 31104000;
		month = floor(aux/2592000);
		aux = aux % 2592000;
		day = floor(aux/86400);
		aux = aux % 86400;
		hour = floor(aux/3600);
		aux = aux % 3600;
		min = floor(aux/60);
		aux = aux % 60;
		sec = aux;
  	}
	pclose_status = pclose(fp);
	if(Debug) printf("Pclose returned %d\n", pclose_status);

	sprintf(lvala, "%02d:%02d:%02d:%02d:%02d:%02d", year, month, day, hour, min, sec);
	//strcpy(lvala, "00:00:00:00:00");

	if(Debug){
		printf("UPtime: %02d:%02d:%02d:%02d:%02d:%02d\n", year, month, day, hour, min, sec);
		printf("Sub processed\n");
	}

	return 0;
}




/* Register these symbols for use by IOC code: */
epicsExportAddress(int, Debug);
epicsRegisterFunction(getwaterdata);
