#!../../bin/linux-x86_64/wld

## You may have to change wld to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/wld.dbd"
wld_registerRecordDeviceDriver pdbbase

## Set global variable
var Debug 0

## Load record instances
dbLoadRecords("db/wld.db","BL=PINK,DEV=WLD")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=epics"
